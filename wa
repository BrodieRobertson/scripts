#!/bin/sh -

parameter=$1
default_folder="~/Videos/Anime"

help() {
    echo -e "wa with any extra parameters will load all the videos in the default folder\nUse -n to load a single video in default folder\nUse -p to load all videos in a specified path\nUse -pn to load a single video in specified folder"
}
open_mpv() {
    file=$1

    # Open all videos in folder
    if [ -z "$file" ]; then
        /c/"Program Files"/mpv/mpv.exe *[.mp4\|.mpv\|.mkv]
    # Open similar named videos in folder
    else
        echo "$file"
        /c/"Program Files"/mpv/mpv.exe *$file*[.mp4\|.mpv\|.mkv]
    fi
}

enter_path() {
    # Prompt user for a path to swap to
    while true; do
        read -p "Enter the path you would like to search for videos in: " answer
        if [ -z "$answer" ]; then
            echo "No path entered"
        else
            cd "$(eval echo $answer)"
            break
        fi
    done
}

load_anime() {
    name=$1

    # No name entered
    if [ -z "$name" ]; then
        # List all videos in current directory
        ls *[.mp4\|.mpv\|.mkv]

        # Prompt user for file name
        while true; do
            read -p "Enter a full or partial file name to load (without an extension): " answer
            if [ -z "$answer" ]; then
                echo "No file name entered"
            else
                open_mpv $answer
                break
            fi
        done
    # Load named file
    else
        open_mpv $name
    fi
}

# Open help
if [ -z "$parameter" ]; then
    cd "$(eval echo $default_folder)"
    open_mpv
# No parameter then open all mainfolder anime
elif [ "$parameter" = "-h" ]; then
    help
# Only open anime with pattern matched name
elif [ "$parameter" = "-n" ]; then
    cd "$(eval echo $default_folder)"
    load_anime $2
# Load all videos in a specified folder
elif [ "$parameter" = "-p" ]; then
    # No path entered
    if [ -z "$2" ]; then
        enter_path
        open_mpv
    # cd into the entered path
    else
        cd $2
        open_mpv
    fi
# Load a pattern matched named video in a folder
elif [ "$parameter" = "-pn" ]; then
    # Process path name
    if [ -z "$2" ]; then
        enter_path
    else
        cd "$2"
    fi

    # Process file name
    if [ -z "$3" ]; then
        load_anime
    else
        open_mpv $3
    fi
else
    echo "Unsupported command, type wa -h for help"
fi
