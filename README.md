## Script Descriptions

### gencd

Generates all of my cd alias for my .bashrc

### irepo

Initializes a git repo with the option of setting a remote

### push

Iterates through all of the remotes for the git repo in the current and pushes changes to them

### report

Generates a pandoc markdown file with the settings I use for creating pdf documents

### wa

Used so I can easily load videos into mpv, it might take a bit of tweaking if you want to use it yourself

## Programs Used

+ mpv
+ git

## Script Dependencies
+ irepo
    + git

+ push
  + git
  
+ wa
  + mpv
